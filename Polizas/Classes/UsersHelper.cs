﻿using Polizas.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;


namespace Polizas.Classes
{
    public class UsersHelper : IDisposable
    {
        public static ApplicationDbContext userContext = new ApplicationDbContext();
        private static PolizaContext db = new PolizaContext();
        public void Dispose()
        {
            userContext.Dispose();
            db.Dispose();
        }

        public static void CheckRole(string role)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(userContext));
            //Check if role exists if not then create it
            try
            {
                if (!roleManager.RoleExists(role))
                {
                    roleManager.Create(new IdentityRole(role));
                }
            }
            catch
            {
                throw;
            }
        }

        internal static void CheckSuperUser(string admin)
        {
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var email = WebConfigurationManager.AppSettings["AdminUser"];
            var password = WebConfigurationManager.AppSettings["AdminPassWord"];
            var userAsp = UserManager.FindByName(email);
            if (userAsp == null)
            {
                CreateUserAsp(email, admin, password);
                return;
            }
            UserManager.AddToRole(userAsp.Id, admin);
        }

        private static void CreateUserAsp(string email, string admin, string password)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var userAsp = new ApplicationUser
            {
                Email = email,
                UserName = email,
            };

            userManager.Create(userAsp, password);
            userManager.AddToRole(userAsp.Id, password);
        }
    }
}