﻿using Polizas.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Polizas.Classes
{
    public class Helper : IDisposable
    {
        private static PolizaContext db = new PolizaContext();
        //When GC pass dispose this Class
        public void Dispose()
        {
            db.Dispose();
        }

        public static List<TipoCobertura> GetTipoCobertura()
        {
            var tipoCobertura = db.TipoCobertura.ToList();
            tipoCobertura.Add(new TipoCobertura
            {
                TipoCoberturaId = 0,
                Nombre= "[Seleccione...]"
            });
            tipoCobertura = tipoCobertura.OrderBy(d => d.Nombre).ToList();
            return tipoCobertura;
        }

        internal static IEnumerable GetTipoRiesgo()
        {
            var tipooRiesgo = db. TipoRiesgo.ToList();
            tipooRiesgo.Add(new  TipoRiesgo
            {
                TipoRiesgoId = 0,
                Nombre = "[Seleccione...]"
            });
            tipooRiesgo = tipooRiesgo.OrderBy(d => d.Nombre).ToList();
            return tipooRiesgo;
        }

        internal static IEnumerable GetPoliza()
        {
            var poliza = db.Poliza.ToList();
            poliza.Add(new Poliza
            {
                PolizaId = 0,
                Nombre = "[Seleccione...]"
            });
            poliza = poliza.OrderBy(d => d.Nombre).ToList();
            return poliza;
        }
    }
}