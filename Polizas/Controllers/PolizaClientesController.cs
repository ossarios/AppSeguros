﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Polizas.Models;

namespace Polizas.Controllers
{
    public class PolizaClientesController : Controller
    {
        private PolizaContext db = new PolizaContext();
        public PolizaClientesController()
        {
            ViewBag.Title = "Poliza Cliente";
        }
        // GET: PolizaClientes
        public ActionResult Index()
        {
            return View(db.PolizaClientes.ToList());
        }

        // GET: PolizaClientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolizaCliente polizaCliente = db.PolizaClientes.Find(id);
            if (polizaCliente == null)
            {
                return HttpNotFound();
            }
            return View(polizaCliente);
        }

        // GET: PolizaClientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PolizaClientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Codigo,CodigoPoliza,CodigoCliente")] PolizaCliente polizaCliente)
        {
            if (ModelState.IsValid)
            {
                db.PolizaClientes.Add(polizaCliente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(polizaCliente);
        }

        // GET: PolizaClientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolizaCliente polizaCliente = db.PolizaClientes.Find(id);
            if (polizaCliente == null)
            {
                return HttpNotFound();
            }
            return View(polizaCliente);
        }

        // POST: PolizaClientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Codigo,CodigoPoliza,CodigoCliente")] PolizaCliente polizaCliente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(polizaCliente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(polizaCliente);
        }

        // GET: PolizaClientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolizaCliente polizaCliente = db.PolizaClientes.Find(id);
            if (polizaCliente == null)
            {
                return HttpNotFound();
            }
            return View(polizaCliente);
        }

        // POST: PolizaClientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PolizaCliente polizaCliente = db.PolizaClientes.Find(id);
            db.PolizaClientes.Remove(polizaCliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
