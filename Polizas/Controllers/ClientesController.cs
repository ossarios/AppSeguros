﻿using Polizas.Classes;
using Polizas.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Polizas.Controllers
{
    public class ClientesController : Controller
    {
        private PolizaContext db = new PolizaContext();

        public ClientesController()
        {
            ViewBag.Title = "Cliente";
        }
        // GET: Clientes
        public ActionResult Index()
        {
            return View(db.Clientes.ToList());
        }

        // GET: Clientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            ViewBag.CodigoPoliza = new SelectList(Helper.GetPoliza(), "Codigo", "Nombre");
            return View();
        }

        // POST: Clientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Codigo,Nombre,Identificacion,Celular,CodigoPoliza,Correo,CodigoPolizaCliente")] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                cliente.Codigo = 0;
                try
                {
                    db.Clientes.Add(cliente);
                    db.SaveChanges();
                }
                catch 
                {
                    throw;
                }
                return RedirectToAction("Index");
            }

            ViewBag.CodigoTipoCobertura = new SelectList(Helper.GetTipoCobertura(), "Codigo", "Nombre", cliente.CodigoPolizaCliente);
            return View(cliente);
        }

        // GET: Clientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }

            ViewBag.CodigoPoliza = new SelectList(Helper.GetPoliza(), "Codigo", "Nombre");
            return View(cliente);
        }

        // POST: Clientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Codigo,Nombre,Identificacion,Celular,CodigoPoliza,Correo,CodigoPolizaCliente")] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cliente).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch
                {
                    throw;
                }
                return RedirectToAction("Index");
            }

            ViewBag.CodigoTipoCobertura = new SelectList(Helper.GetTipoCobertura(), "Codigo", "Nombre", cliente.CodigoPolizaCliente);
            return View(cliente);
        }

        // GET: Clientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Cliente cliente = db.Clientes.Find(id);
                db.Clientes.Remove(cliente);
                db.SaveChanges();
            }
            catch
            {

                throw;
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
