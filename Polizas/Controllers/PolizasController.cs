﻿using Polizas.Classes;
using Polizas.Models;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Polizas.Controllers
{
    public class PolizasController : Controller
    {
        private PolizaContext db = new PolizaContext();

        public PolizasController()
        {
            ViewBag.TipoCoberturaPoliza = db.Poliza.Include(c => c.TipoCoberturaPoliza);
            ViewBag.TipoRiesgoPoliza   = db.Poliza.Include(c => c.TipoRiesgoPoliza);

            ViewBag.Title = "Poliza";
        }
        // GET: Polizas
        public ActionResult Index()
        {
            return View(db.Poliza.ToList());
        }

        // GET: Polizas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poliza poliza = db.Poliza.Find(id);
            if (poliza == null)
            {
                return HttpNotFound();
            }
            return View(poliza);
        }

        // GET: Polizas/Create
        public ActionResult Create()
        {
            ViewBag.TipoCoberturaId = new SelectList(Helper.GetTipoCobertura(), "TipoCoberturaId", "Nombre");
            ViewBag.TipoRiesgoId    = new SelectList(Helper.GetTipoRiesgo(), "TipoRiesgoId", "Nombre");
            return View();
        }

        // POST: Polizas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Codigo,Nombre,Descripcion,CodigoTipoCobertura,Inicio,Periodo,Precio,CodigoTipoRiesgo,CodigoPolizaCliente")] Poliza poliza)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Poliza.Add(poliza);
                    db.SaveChanges();
                }
                catch
                {
                    throw;
                }
                return RedirectToAction("Index");
            }

            ViewBag.TipoCoberturaId  = new SelectList(Helper.GetTipoCobertura(), "Codigo", "Nombre", poliza.TipoCoberturaPoliza.TipoCoberturaId);
            ViewBag.TipoRiesgoId = new SelectList(Helper.GetTipoRiesgo(), "Codigo", "Nombre",        poliza.TipoRiesgoPoliza.TipoRiesgoId);            
            return View(poliza);
        }

        // GET: Polizas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poliza poliza = db.Poliza.Find(id);
            if (poliza == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoCoberturaId = new SelectList(Helper.GetTipoCobertura(), "Codigo", "Nombre");
            ViewBag.TipoRiesgoId = new SelectList(Helper.GetTipoRiesgo(), "Codigo", "Nombre",       poliza.TipoRiesgoId);
            return View(poliza);
        }

            
        // POST: Polizas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Codigo,Nombre,Descripcion,CodigoTipoCobertura,Inicio,Periodo,Precio,CodigoTipoRiesgo,CodigoPolizaCliente")] Poliza poliza)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(poliza).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch 
                {
                    throw;
                }
                return RedirectToAction("Index");
            }
            ViewBag.TipoCoberturaId = new SelectList(Helper.GetTipoCobertura(), "TipoCoberturaId", "Nombre", poliza.TipoCoberturaId);
            ViewBag.TipoRiesgoId = new SelectList(Helper.GetTipoRiesgo(), "TipoRiesgoId", "Nombre", poliza.TipoRiesgoId);
            return View(poliza);
        }

        // GET: Polizas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poliza poliza = db.Poliza.Find(id);
            if (poliza == null)
            {
                return HttpNotFound();
            }
            return View(poliza);
        }

        // POST: Polizas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Poliza poliza = db.Poliza.Find(id);
                db.Poliza.Remove(poliza);
                db.SaveChanges();
            }
            catch
            {

                throw;
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
