﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Polizas.Models;

namespace Polizas.Controllers
{
    public class TipoRiesgosController : Controller
    {
        private PolizaContext db = new PolizaContext();

        public TipoRiesgosController()
        {
            ViewBag.Title = "Tipo Riesgo";
        }
        // GET: TipoRiesgos
        public ActionResult Index()
        {
            return View(db.TipoRiesgo.ToList());
        }

        // GET: TipoRiesgos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoRiesgo tipoRiesgo = db.TipoRiesgo.Find(id);
            if (tipoRiesgo == null)
            {
                return HttpNotFound();
            }
            return View(tipoRiesgo);
        }

        // GET: TipoRiesgos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoRiesgos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Codigo,Nombre,Descripcion")] TipoRiesgo tipoRiesgo)
        {
            if (ModelState.IsValid)
            {
                db.TipoRiesgo.Add(tipoRiesgo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoRiesgo);
        }

        // GET: TipoRiesgos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoRiesgo tipoRiesgo = db.TipoRiesgo.Find(id);
            if (tipoRiesgo == null)
            {
                return HttpNotFound();
            }
            return View(tipoRiesgo);
        }

        // POST: TipoRiesgos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Codigo,Nombre,Descripcion")] TipoRiesgo tipoRiesgo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoRiesgo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoRiesgo);
        }

        // GET: TipoRiesgos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoRiesgo tipoRiesgo = db.TipoRiesgo.Find(id);
            if (tipoRiesgo == null)
            {
                return HttpNotFound();
            }
            return View(tipoRiesgo);
        }

        // POST: TipoRiesgos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoRiesgo tipoRiesgo = db.TipoRiesgo.Find(id);
            db.TipoRiesgo.Remove(tipoRiesgo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
