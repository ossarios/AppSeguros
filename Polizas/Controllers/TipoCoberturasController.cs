﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Polizas.Models;

namespace Polizas.Controllers
{
    public class TipoCoberturasController : Controller
    {
        private PolizaContext db = new PolizaContext();

        public TipoCoberturasController()
        {
            ViewBag.Title = "Tipo Coberturas";
        }
        // GET: TipoCoberturas
        public ActionResult Index()
        {
            return View(db.TipoCobertura.ToList());
        }

        // GET: TipoCoberturas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoCobertura tipoCobertura = db.TipoCobertura.Find(id);
            if (tipoCobertura == null)
            {
                return HttpNotFound();
            }
            return View(tipoCobertura);
        }

        // GET: TipoCoberturas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoCoberturas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Codigo,Nombre,Descripcion")] TipoCobertura tipoCobertura)
        {
            if (ModelState.IsValid)
            {
                db.TipoCobertura.Add(tipoCobertura);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoCobertura);
        }

        // GET: TipoCoberturas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoCobertura tipoCobertura = db.TipoCobertura.Find(id);
            if (tipoCobertura == null)
            {
                return HttpNotFound();
            }
            return View(tipoCobertura);
        }

        // POST: TipoCoberturas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Codigo,Nombre,Descripcion")] TipoCobertura tipoCobertura)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoCobertura).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoCobertura);
        }

        // GET: TipoCoberturas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoCobertura tipoCobertura = db.TipoCobertura.Find(id);
            if (tipoCobertura == null)
            {
                return HttpNotFound();
            }
            return View(tipoCobertura);
        }

        // POST: TipoCoberturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoCobertura tipoCobertura = db.TipoCobertura.Find(id);
            db.TipoCobertura.Remove(tipoCobertura);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
