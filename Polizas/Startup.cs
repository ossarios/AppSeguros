﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Polizas.Startup))]
namespace Polizas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
