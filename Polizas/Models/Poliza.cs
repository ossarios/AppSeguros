﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Polizas.Models
{
    public class Poliza
    {
        [Key]
        public int PolizaId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage ="El campo {0} es de 50 caracteres")]
        [Index("Poliza_Nombre_Index", IsUnique = true)]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} es de 50 caracteres")]
        public string Descripcion { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Cobertura")]
        public int TipoCoberturaId { get; set; }
        public virtual TipoCobertura TipoCoberturaPoliza { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Inicio")]
        public DateTime Inicio { get; set; } = DateTime.Now;

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, 12, ErrorMessage = "Seleccione un {0} entre 1 y 12")]
        public int Periodo { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        public double Precio { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Riesgo")]
        public int TipoRiesgoId { get; set; }
        public virtual TipoRiesgo TipoRiesgoPoliza{ get; set; }

        [Display(Name = "Poliza Cliente")]
        public int CodigoPolizaCliente { get; set; } = 0;
        public virtual ICollection<PolizaCliente> PolizaCliente { get; set; }       
    }
}