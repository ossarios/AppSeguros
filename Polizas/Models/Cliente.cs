﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Polizas.Models
{
    public class Cliente
    {
        [Key]
        public int Codigo { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} es de 50 caracteres")]
        public string Nombre { get; set; } [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} es de 50 caracteres")]
        public string Identificacion { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} es formato celular")]
        [DataType(DataType.PhoneNumber)]
        public string Celular{ get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public int CodigoPoliza { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.EmailAddress)]
        public string Correo { get; set; }
        [Display(Name = "Codigo")]
        public int CodigoPolizaCliente { get; set; }
        public virtual ICollection<PolizaCliente> PolizaCliente { get; set; }
    }
}