﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Polizas.Models
{
    public class TipoCobertura
    {
        [Key]
        public int TipoCoberturaId { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} es de 50 caracteres")]
        [Index("TipoCobertura_Nombre_Index", IsUnique = true)]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} es de 50 caracteres")]
        public string Descripcion { get; set; }
        public int PolizaId { get; set; }
        public virtual ICollection<Poliza> Poliza{ get; set; }
    }
}