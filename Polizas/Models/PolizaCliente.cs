﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Polizas.Models
{
    public class PolizaCliente
    {
        [Key]
        public int Codigo { get; set; }
        [Index("Poliza_CodigoPoliza_Index", IsUnique = true)]
        public int CodigoPoliza { get; set; }
        [Index("Poliza_CodigoCliente_Index", IsUnique = true)]
        public int CodigoCliente { get; set; }
        public virtual Poliza Poliza { get; set; }
        public virtual Cliente Cliente { get; set; }
    }
}