﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Polizas.Models
{
    public class PolizaContext: DbContext
    {
        public PolizaContext(): base("DefaultConnection")
        {
                            
        }

        public System.Data.Entity.DbSet<Polizas.Models.Poliza> Poliza { get; set; }
        public System.Data.Entity.DbSet<Polizas.Models.TipoCobertura> TipoCobertura { get; set; }
        public System.Data.Entity.DbSet<Polizas.Models.TipoRiesgo> TipoRiesgo { get; set; }

        public System.Data.Entity.DbSet<Polizas.Models.Cliente> Clientes { get; set; }

        public System.Data.Entity.DbSet<Polizas.Models.PolizaCliente> PolizaClientes { get; set; }
    }
}